﻿using UnityEngine;
using System.Collections;

public class CardFlipping : MonoBehaviour {

	private static ILogger logger = Debug.logger;
	private static string kTAG = "CARDS";

	public Transform from;
	public Quaternion to;
	public float speed = 2F;
	public bool isRotating = false;
	public bool isDragging = false;
	public bool back = true;
	private float distance;

	void Awake() {
		Application.targetFrameRate = -1;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (isRotating) {
			logger.Log(kTAG, "from: " + transform.rotation + " to: " + to + " diff: " + Quaternion.Angle(transform.rotation, to));
			transform.rotation = Quaternion.Lerp (transform.rotation, to, Time.deltaTime * speed);
		}
		if(Quaternion.Angle(transform.rotation, to) == 0F) {
			isRotating = false;
		}
		if (isDragging) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Vector3 rayPoint = ray.GetPoint(distance);
			rayPoint.z = transform.position.z;
			transform.position = rayPoint;
		}
	}

	void OnMouseUp () {
		if (isDragging) {
			logger.Log (kTAG, "Dragged");
			isDragging = false;
//		} else {
//			logger.Log (kTAG, "Clicked");
//			flipCard ();
		}
	}

	void OnMouseUpAsButton() {
		logger.Log (kTAG, "clicked");
		flipCard ();
	}

	void OnMouseDrag () {
		if (!isRotating) {
			logger.Log (kTAG, "dragging");
			isDragging = true;
			distance = Vector3.Distance(transform.position, Camera.main.transform.position);
		}
	}

	private void flipCard() {
		isRotating = true;
		to = Quaternion.Euler(0, back ? 180 : 0, 0);
		back = !back;
	}
}
